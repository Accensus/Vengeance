AddKeySection "Vengeance" VengeanceKeys
AddMenuKey "Frailty" "use SkillOffensiveOne"
AddMenuKey "Vengeance" "use SkillOffensiveTwo"
AddMenuKey "Blink" "use SkillSupportOne"
AddMenuKey "Ignore Pain" "use SkillSupportTwo"