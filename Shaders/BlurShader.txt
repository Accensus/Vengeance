// Bokeh disc.
// by David Hoskins.
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// The Golden Angle is (3.-sqrt(5.0))*PI radians, which doesn't precompiled for some reason.
// The compiler is a dunce I tells-ya!!

// modified for gzdoom use by kodi. Don't blame David Hoskins for any errors I might have caused.
#define GOLDEN_ANGLE 2.39996

#define ITERATIONS 100

mat2 rot = mat2(cos(GOLDEN_ANGLE), sin(GOLDEN_ANGLE), -sin(GOLDEN_ANGLE), cos(GOLDEN_ANGLE));

//-------------------------------------------------------------------------------------------
vec3 Bokeh(sampler2D tex, vec2 uv, float radius)
{
    vec3 acc = vec3(0), div = acc;
    float r = 1.;
    vec2 vangle = vec2(0.0,radius*.01 / sqrt(float(ITERATIONS)));

    for (int j = 0; j < ITERATIONS; j++)
    {
        // the approx increase in the scale of sqrt(0, 1, 2, 3...)
        r += 1. / r;
        vangle = rot * vangle;
        vec3 col = texture(tex, uv + (r-1.) * vangle).xyz; // / ... Sample the image
       // col = col * col *1.8; // ... Contrast it for better highlights - leave this out elsewhere.
        vec3 bokeh = pow(col, vec3(4));
        acc += col * bokeh;
        div += bokeh;
    }
    return acc / div;
}


//-------------------------------------------------------------------------------------------
void main()
{
    float rad = .8 - .8*cos(factor * 6.283);
    vec2 dif = vec2(TexCoord.x-0.5, TexCoord.y-0.5); // difference between current texcoord and the middle of the screen
    // float ang = atan(dif.y,   dif.x); // angle from middle of screen to texcoord
    float dist = length(dif); // distance of the same
    vec4 color = texture(InputTexture,TexCoord);// input new coordinates
    vec4 BokehColor = vec4(Bokeh(InputTexture, TexCoord, rad), 1.0);
    vec4 mixed = mix(color, BokehColor, min(0.15+dist/0.5,1.0));

    // FragColor = mix(vec4(c_rgb.rgb,color.a),mixed,0);

    FragColor = clamp (mixed, 0, 1);
}